const express = require('express');
const morgan = require('morgan');

// Iniciamos el servidor
const app = express();

// Iniciamos los modelos de sequelize
const db = require('./db');

// Sincronizamos la base de datos
db.sequelize.sync().done(() => {
  console.log("\n***Base de datos generada");
});

// Habilitamos los logs
app.use(morgan("dev"));

app.use((req, res, next) => {
  console.log("***Procesando petición...\n");
  crearPersonaYHobbie();
  next();
});

app.get("*", (req, res) => {
  res.end("***Aplicación NODEJS iniciada en EXPRESSJS\n");
});


function crearPersona() {
  const persona = {
    nombres: 'JUAN',
    apellidos: 'PEREZ PEREZ',
    fecha_nacimiento: new Date(),
  };

  db.persona.create(persona)
  .then(respuesta => {
    console.log("\n***creando persona");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));
}

function crearPersonaYHobbie() {
  const persona = {
    nombres: 'MARIA',
    apellidos: 'SUAREZ SUAREZ',
    fecha_nacimiento: new Date(1990, 11, 31),
  };

  db.persona.create(persona)
  .then(respuesta => {
    console.log("\n***creando persona para hobbie");
    console.log(JSON.stringify(respuesta));

    const hobbie = {
      descripcion: 'Pasear en  bicicleta',
      fid_persona: respuesta.id_persona,
    } 

    return db.hobbie.create(hobbie);
  }).then(respuesta => {
    console.log("\n***creando hobbie");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));
}


function modificarPersona() {

  db.persona.update({
    nombres: 'MARIA ISABEL',
  }, {
    where: {
      id_persona: 2,
    },
    // returning: true,
  })
  .then(respuesta => {
    console.log("\n***modificando persona");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}


function modificarPersonaObjeto() {

  db.persona.findById(2)
  .then(respuesta => {
    return respuesta.updateAttributes({ nombres: 'MARIA ISABLE' });
  }).then(respuesta => {
    console.log("\n***modificando persona");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}


function listarPersonas() {

  db.persona.findAll()
  .then(respuesta => {
    console.log("\n***Listando persona");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}



function listarPersonasYHobbies() {

  db.persona.findAll({
    include: [{
      model: db.hobbie,
      as: 'hobbies',
    }],
  })
  .then(respuesta => {
    console.log("\n***Listando persona y hobbies");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}

function listarHobbies() {

  db.hobbie.findAll({
    include: [{
      model: db.persona,
      as: 'persona',
    }],
  })
  .then(respuesta => {
    console.log("\n***Listando hobbies");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}


function borrarHobbie() {

  db.hobbie.destroy({
    where: {id_hobbie: 2},
  })
  .then(respuesta => {
    console.log("\n***Eliminando hobbie");
    console.log(JSON.stringify(respuesta));
  }).catch(error => console.log(error));

}


app.listen(4000);